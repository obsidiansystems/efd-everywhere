{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
module T10675_Conflicting where

class CX_FD0Class a where
  type CX_FD0 a

class (CX_FD0Class a, CX_FD0 a ~ b) => CX x a b where
  op :: x -> a -> b

instance CX_FD0Class [x] where
  type CX_FD0 [x] = [x]
instance CX Bool [x] [x]

-- Perhaps OK if had had a backtracking version of =>
instance CX_FD0Class x => CX_FD0Class [x] where
  type CX_FD0 [x] = [Maybe (CX_FD0 x)]
instance (CX Char x y) => CX Char [x] [Maybe y]

f x = op True [x]
