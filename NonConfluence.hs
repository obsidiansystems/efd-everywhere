{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
module NonConfluence where

type family FD a
class FD b ~ c => D a b c

-- D instances rejected as FD instances never match

type instance FD Int = (Int, Int)
instance {-# LIBERAL #-} (q ~ Int)  => D Int  p (Int,q)

type instance FD Bool = (Bool, Bool)
instance {-# LIBERAL #-} (s ~ Bool) => D Bool r (s,Bool)
