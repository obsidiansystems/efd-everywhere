{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}

module NormalAdd where

data Nat = Z | S Nat

type family AddInv0 (a :: Nat) (b :: Nat) :: Nat
type family AddInv1 (a :: Nat) (b :: Nat) :: Nat

class ( AddInv0 a (Add a b) ~ b
      -- we got half way there
      --, AddInv1 (Add a b) b ~ a
      ) => AddClass (a :: Nat) (b :: Nat) where
  type Add a b :: Nat

instance AddClass Z b where
  type Add Z b = b
instance AddClass a b => AddClass (S a) b where
  type Add (S a) b = S (Add a b)

type instance AddInv0 Z r = r
type instance AddInv0 (S a) (S r) = AddInv0 a r

type instance AddInv1 r Z = r
type instance AddInv1 (S r) (S a) = AddInv1 r a
