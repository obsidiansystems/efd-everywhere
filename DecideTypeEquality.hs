{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}

module DecideTypeEquality where

type family TypeEqTF a b where
  TypeEqTF a a = True
  TypeEqTF _ _ = False

class TypeEqTF a b ~ r => TypeEq a b r
instance TypeEqTF a b ~ r => TypeEq a b r
