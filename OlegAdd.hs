{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

-- | How Oleg makes it go
module OlegAdd where

data Nat = Z | S Nat

---- three FunDeps

class ( OlegAdd'_FD0 n m ~ r -- N.B. prime, reusing FD witness
      , OlegAdd'_FD1 n r ~ m -- N.B. prime, reusing FD witness
      , OlegAdd'_FD1 m r ~ n -- N.B. prime, reusing FD witness
      , OlegAdd' n m r, OlegAdd' m n r)
      => OlegAdd (n :: Nat) (m :: Nat) (r :: Nat)
instance (OlegAdd' n m r, OlegAdd' m n r)
      => OlegAdd n m r

-- two FunDeps

type family OlegAdd'_FD0 (n :: Nat) (m :: Nat) :: Nat
type family OlegAdd'_FD1 (n :: Nat) (r :: Nat) :: Nat

class ( OlegAdd'_FD0 n m ~ r
      , OlegAdd'_FD1 n r ~ m
      ) => OlegAdd' (n :: Nat) (m :: Nat) (r :: Nat)

type instance OlegAdd'_FD0 Z m = m
type instance OlegAdd'_FD0 (S n) m = S (OlegAdd'_FD0 n m)

type instance OlegAdd'_FD1 Z r = r
type instance OlegAdd'_FD1 (S n) (S r) = OlegAdd'_FD1 n r

instance (m ~ r) => OlegAdd' Z m r

instance ((S r') ~ r, OlegAdd' n' m r') => OlegAdd' (S n') m r
