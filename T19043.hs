{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE TypeOperators #-}

module T19043 where

import Data.Type.Equality

type family FInv a

class (FInv (F a) ~ a) => FClass a where
  type F a

useInjectivity :: (FClass a, FClass b, F a ~ F b) => a :~: b
useInjectivity = Refl
