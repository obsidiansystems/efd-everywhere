{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
module Nontermination where

type family MulRes a b
class MulRes a b ~ c => Mul a b c

data Vec a = Vec a a a

type instance MulRes a (Vec b) = Vec (MulRes a b)
instance Mul a b c => Mul a (Vec b) (Vec c)

f :: Mul alpha (Vec beta) beta => (alpha, beta)
f = (undefined, undefined)

-- solver overflows stack
g :: (alpha, beta)
g = f
