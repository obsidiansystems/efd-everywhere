{-# LANGUAGE TypeFamilies #-}

-- | W1 from "Injective Type Families"
module W1 where

type family FInv a

class (FInv (F a) ~ a) => FClass a where
  type F a

----

type instance FInv a = [a]

instance FClass [a] where
  type F [a] = a
