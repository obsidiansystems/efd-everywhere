{-# LANGUAGE FunctionalDependencies, FlexibleInstances, UndecidableInstances,
             ScopedTypeVariables, TypeFamilies, TypeApplications,
             FlexibleContexts, AllowAmbiguousTypes #-}

module T18851_CTF where

class C_FD_Class a where
  type C_FD a
class (C_FD_Class a, C_FD a ~ b) => C a b

instance C_FD_Class Int where
   type C_FD Int = Bool

-- 'g' diverges at runtime
instance (C Int b) => C Int b
---- compile time errors
--instance C Int b

class IsInt int
instance int ~ Int => IsInt int

data A
instance Show A where
  show _ = "A"
data B
instance Show B where
  show _ = "B"

f :: forall a b c int
  .  ( Show a, Show b
     , Show c -- C_FD Int = Bool makes this happy
     , C int a, C int b, C int c
     , IsInt int
     -- , c ~ C_FD int -- add this to get rid of ambiguity error
     )
  => String
f = show (undefined :: c)

g = f @A @B
