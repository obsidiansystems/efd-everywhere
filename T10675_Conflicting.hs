{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
module T10675_Conflicting where

type family CX_FD0 a

class CX_FD0 a ~ b => CX x a b where
  op :: x -> a -> b

type instance CX_FD0 [x] = [x]
instance CX Bool [x] [x]

-- conflicting head
type instance CX_FD0 [x] = [Maybe (CX_FD0 x)]
instance CX Char x y => CX Char [x] [Maybe y]

f x = op True [x]
