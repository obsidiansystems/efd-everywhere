{-# LANGUAGE DataKinds #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

module AntCAdd where

data Nat = Z | S Nat

class (FAddab a b ~ c, FSubtac a c ~ b, FSubtbc b c ~ a) => AddTF a b c
instance (FAddab a b ~ c, FSubtac a c ~ b, FSubtbc b c ~ a) => AddTF a b c

type family FAddab (a :: Nat) (b :: Nat) :: Nat
type instance FAddab Z      b = b
type instance FAddab (S a') b = S (FAddab a' b)

-- subtract a from c, by recursive descent on a
type family FSubtac (a :: Nat) (c :: Nat) :: Nat
type instance FSubtac Z      c      = c
type instance FSubtac (S a') (S c') = FSubtac a' c'

-- subtract b from c, by type eq test??
type family FSubtbc b c where
  FSubtbc b b      = Z
  FSubtbc b (S c') = S (FSubtbc b c')
