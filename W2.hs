{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE UndecidableInstances #-}

-- | from "Injective Type Families"
module W2 where

type family GInv a

class (GInv (G a) ~ a) => GClass a where
  type G a

instance (GInv (G a) ~ [a], GClass a) => GClass [a] where
  type G [a] = G a

-- A little thinking shows us that `μ a. [a]` would work, if we had it, and
-- indeed GHC agrees (counterfactually):

class (GInv (G a) ~ a) => G'Class a where
  type G' a

instance (a ~ [a], GClass a) => G'Class [a] where
  type G' [a] = G a
